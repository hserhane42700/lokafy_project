# Generated by Django 3.0.2 on 2020-01-20 09:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('review', '0002_post_cover'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='cover',
        ),
    ]
